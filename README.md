# Projet Programmation Avancée S6 | IMA 3

Période de conception : du 01-04-2020 au 29-05-2020

Auteurs du projet :  
\- Jean Baumann  
\- Gaétan Simonin

Ce dépôt `GitLab` contient le projet `C` sur lequel nous avons travaillé durant notre 6e semestre d'études supérieures dans le cadre de notre formation d'ingénieur en Système Embarqué (anciennement IMA) à Polytech' Lille. N'hésitez pas à cloner ce dépôt et l'adapter à vos besoins.

## Présentation du projet

Pour effectuer une analyse de la qualité de l’air, une association a déployé 449 capteurs de pollution dans la ville de Århus (Danemark). Ces capteurs connectés ont effectué des mesures pendant 2 mois (du 1er août 2014 au 1er octobre 2014). Les données mesurées ont ensuite été compilés dans des fichiers et rendus publics. Le projet proposé consiste à analyser les données récoltées sous forme d’un programme fonctionnant en ligne de commandes.

## Contenu du dépôt
- `README.md` : le fichier que vous être en train de lire, présente bièvement le projet et l'utilité des fichiers qui le compose. 
- `Makefile` : permet la compilation "automatique" du code.
- `.clang-format` : contient les règles de formatage de code.
- `src/` : le dossier qui contient le code source du projet.
	- `cmd_reader.c` : contient les fonctions de lecture des commandes reçues dans le terminal.
	- `file_reader.c` contient les fonctions de lecture des fichiers `.csv`, les fonctions de remplissage de la structure de données et les fonctions du processus de recherche.
	- `main.c` contient la fonction principale du programme.
- `includes/` : le dossier contient les déclarations des fonctions qu'utilise les codes sources
	- `cmd_reader.h` : contient les prototypes des fonctions de `cmd_reader.c`.
	- `file_reader.h` : contient les prototypes des fonctions de `file_reader.c`.
- `commands.txt` : contient des exemples de commandes pouvant être passées au programme. Il sert aussi à automatiser le test (voir le `Makefile`).
- `Rapport_Projet_PA_S6.pdf` : présente en détails le fonctionnement du programme et la démarche adoptée pour concrétiser le projet.

## Le fichier `Makefile`
Le fichier `Makefile` permet d'automatiser la compilation ou le lancement du programme. Voici ses fonctionalités : 

Téléchargement du set de data :  
\- Télécharger le dossier `dataset/` contenant les data (Ecrire : make zip).  

Lancement manuel du programme : (avec vos fichiers de données)  
\- Uniquement compiler le programme (Ecrire : `make`).  
\- Uniquement compiler le programme avec tous les warnings possibles, mode Warning (Ecrire : `make all`).  

Lancement automatique du programme : (nécessite le dossier `dataset/`)  
\- Compiler et lancer le programme simplement, en mode Release (Ecrire : `make m`).  
\- Compiler et lancer le programme en mode Batch (nécessite le fichier `commands.txt`) (Ecrire : `make b`).  
\- Compiler et lancer le programme en mode Debug avec gdb (Ecrire : `make gdb`).  
\- Compiler et lancer le programme en mode Memory Check avec valgrind (Ecrire : `make val`).  

Suppression de fichiers ou de dossiers :  
\- Supprimer le dossier `dataset/` (Ecrire : `make czip`).  
\- Supprimer les fichiers intermédiaires et les bibliothèques (Ecrire : `make c`).  
\- Supprimer les fichiers intermédiaires, les bibliothèques et l'executable (Ecrire : `make clean`).  
\- Supprimer tous les fichiers et dossiers qui ne sont pas du code source ou des headers (Ecrire : `make mrpropper`).  

