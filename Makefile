###################### VARIABLES #################################

# Compilateur
	cc = gcc
# Executable
	exec = main


# Option de compilation pour la librairie mathématiques
	mathOption = -lm
# Options de compilation
	options = 


# Librairie
	lib = file_reader.a cmd_reader.a
# Compilateur de librairies
	lib-cc = ar r


# Chemin du dossier contenant les fichiers sources (.c)
	srcPath = ./src/

# Nom du dossier contenant les fichiers headers (.h)
	hdrPath = ./includes/

# Fichier de commandes pour le mode Batch
	file =

# Dossier contenant les données à traiter
	dir = ./dataset/pollution/

# Dependances
	dep = $(srcPath)$(exec).c $(lib)


# Lanceur d'application : gdb
	launcher =

# Mode de lancement de l'application
	launch = automatique

# Mode de compilation
	mode = Release

###################### COMMANDS #################################

# Lancement manuel du programme : (avec vos fichiers de données) 
# Attention ces modes effectuent seulement la compilation

# Uniquement compiler le programme simplement, en mode Release (Ecrire : make)
default: launch = manuel
default: print-mode-compil $(exec) c print-how-to-launch

# Lancer le programme avec tous les warnings possibles, mode Warning (Ecrire : make all)
# L'utilisation de -Wno-padded permet d'éviter l'affichage des warnings dû au padding des structures
all: cc = clang
all: options = -Weverything -Wno-padded
all: mode = Warning 
all: launch = manuel
all: clang-format-tidy default

# Lancement automatique du programme : (nécessite le dossier ./dataset/pollution/) 

# Compiler et lancer le programme simplement, en mode Release (Ecrire : make m).
m: print-mode-compil $(exec) c application-launch

# Compiler et lancer le programme en mode Batch (nécessite le fichier commands.txt) (Ecrire : make b).
b: mode = Batch
b: file = commands.txt
b: m

# Compiler et lancer le programme en mode Debug avec gdb (Ecrire : make gdb).
gdb: mode = Debug
gdb: options = -W -Wall -Wextra -pedantic -g -O0
gdb: dir = 
gdb: launcher = gdb
gdb: m

# Compiler et lancer le programme en mode Debug avec valgrind (Ecrire : `make val`).
val: mode = Memory Check
val: options = -W -Wall -Wextra -pedantic -g -O0
val: launcher = valgrind
val: m

# Télécharger le fichier zip dataset (Ecrire : make zip)
zip: dataset/pollution.zip

# Supprimer le dossier dataset (Ecrire : make czip)
czip: clean-dataset

# Supprimer les fichiers intermédiaires et les bibliothèques créées (Ecrire : make c)
c: clean-auxiliary-files

# Supprimer les fichiers intermédiaires et l'application (Ecrire : make clean)
clean: c clean-executable

# Supprimer tous les fichiers puvant être obtenu avec le Makefile (Ecrire : make mrpropper)
mrpropper: czip clean

##################### WORKING COMMANDS DETAILED #################################

print-mode-compil:
	@echo "\nGénération en mode $(mode)\n\n"

print-mode-launch:
	@echo "\nLancement du programme : $(launch)"

print-how-to-launch: print-mode-launch
	@echo "Entrez la commande : ./$(exec) repDonnées [fichier.txt]\n"

cmd_reader: $(srcPath)cmd_reader.c $(hdrPath)cmd_reader.h
		@ # compilation du cmd_reader.o
	$(cc) -c $< $(options)

		@ # création de la librairie cmd_reader.a
	$(lib-cc) $@.a $@.o 
	@echo "Bibliothèque : cmd_reader.a créée avec succès\n"

file_reader: $(srcPath)file_reader.c $(hdrPath)file_reader.h cmd_reader.a
		@ # compilation du file_reader.o
	$(cc) -c $< $(options)

		@ # affiliation de file_reader.o à la librairie cmd_reader.a
	$(lib-cc)cs cmd_reader.a $@.o

		@ # création de la librairie file_reader.a
	$(lib-cc) $@.a $@.o 
	@echo "Bibliothèque : file_reader.a créée avec succès\n"

$(exec): cmd_reader file_reader $(dep)
		@ # compilation du main.o
	$(cc) -c $(srcPath)$@.c $(options)

		@ # affiliation de main.o à la librairie file_reader.a
	$(lib-cc)cs file_reader.a $@.o
	$(lib-cc)cs cmd_reader.a $@.o
	@echo "\n"

		@ # compilation de l'executable
	$(cc) -o $@ $@.o $(lib) $(mathOption)
	@echo "Application main compilée avec succès\n"

application-launch: print-mode-launch $(exec)
		@ # Lancement du programme
	$(launcher) ./$(exec) $(dir) $(file)

clang-format-tidy:
		@ # clang-format et clang-tidy
	clang-format -i -style=file $(srcPath)*.c $(hdrPath)*.h
	@echo "\n"
	clang-tidy --checks="*" $(srcPath)*.c $(hdrPath)*.h
	@echo "\n"

###################### DATA COLLECTION COMMANDS #################################

dataset/pollution.zip:
	mkdir -p dataset
	test -f $@ || wget https://www.dequidt.me/upload/ima3/pollution.zip --output-document=$@
	unzip dataset/pollution.zip -x __MACOSX/* -d dataset

###################### CLEANING COMMANDS #################################

clean-dataset:
	rm -fr dataset

clean-auxiliary-files:
	rm -fr *.o
	rm -fr *.a

clean-executable:
	rm -fr $(exec)
