/* *************** FILE_READER.C ******************** */
//
//      Contient les fonctions relatives à l'ouverture, la lecture et le
//      stockage des données des fichiers .csv dans la Structure de Données
//      Contient aussi les fonctions de recherche
//
/* ************************************************** */

#include "../includes/file_reader.h"

// _______________________________________________________________________________________________________________
// Fonctions relatives à la Structure de Données

void initSD (Capteur *t)
{ // Initialise la structure de données

    for (int s = 0; s < NB_SENSORS; s++)
    { // Structure Capteur
        t[s].D = malloc (NB_DAYS * sizeof (Day));

        for (int d = 0; d < NB_DAYS; d++)
        { // Structure Jour
            t[s].D[d].H = malloc (NB_MEASURES_DAY * sizeof (Hour));

            for (int h = 0; h < NB_MEASURES_DAY; h++)
            { // Structure Hour
                t[s].D[d].H[h].M    = calloc (NB_GAS, sizeof (int));
                t[s].D[d].H[h].hour = calloc (S_HOUR, sizeof (char));
            }

            t[s].D[d].date       = calloc (S_DATE, sizeof (char));
            t[s].D[d].nbMeasures = 0;
        }

        t[s].lon    = (long double)0.;
        t[s].lat    = (long double)0.;
        t[s].nbDays = 0;
    }
}

void desallocSD (Capteur *t, int *nbSensors)
{ // Desalloue correctement la SD

    for (int s = 0; s < NB_SENSORS; s++)
    { // Structure Capteur

        for (int d = 0; d < NB_DAYS; d++)
        { // Structure Jour

            for (int h = 0; h < NB_MEASURES_DAY; h++)
            { // Structure Hour
                free (t[s].D[d].H[h].M);
                t[s].D[d].H[h].M = NULL;
                free (t[s].D[d].H[h].hour);
                t[s].D[d].H[h].hour = NULL;
            }
            free (t[s].D[d].H);
            t[s].D[d].H = NULL;

            free (t[s].D[d].date);
            t[s].D[d].date       = NULL;
            t[s].D[d].nbMeasures = 0;
        }
        free (t[s].D);
        t[s].D = NULL;

        t[s].lon    = (long double)0.;
        t[s].lat    = (long double)0.;
        t[s].nbDays = 0;
    }
    free (t);
    t          = NULL;
    *nbSensors = 0;
}

void resizing (Capteur *capteurs, int nbSensors)
{ // Redefinie la taille de la SD en fonction des cases remplies (lors de SD_filler)
    // N'est pas opérationnelle (beaucoup de fuites de mémoire)

    for (int s = 0; s < NB_SENSORS; s++)
    { // Structure Capteur

        for (int d = 0; d < NB_DAYS; d++)
        { // Structure Jour

            if (capteurs[s].D[d].nbMeasures < NB_MEASURES_DAY)
            { // Réalloc du tableau des heures de prises de mesures
                // Desallocation des espaces de données non remplis
                capteurs[s].D[d].H = realloc (capteurs[s].D[d].H, (unsigned long)capteurs[s].D[d].nbMeasures);
            }
        }

        if (capteurs[s].nbDays < NB_DAYS)
        { // Réalloc du tableau des dates de prises de mesures

            capteurs[s].D = realloc (capteurs[s].D, (unsigned long)capteurs[s].nbDays);
        }
    }
    if (nbSensors < NB_SENSORS)
    { // Réalloc du tableau de capteurs
        capteurs = realloc (capteurs, (unsigned long)nbSensors);
    }
}

void SD_filler (FILE *fp, Capteur *t, int sensor)
{ // Remplit la SD avec le contenu des fichiers .csv (exclusivement)

    char first_line[S_FIRST_LINE] = "";
    int  day = 0, hour = 0;

    // Lit la première ligne sans qu'elle ne soit intégrée au tableau
    // Permet aussi de savoir si le fichier est vide
    if (fgets (first_line, S_FIRST_LINE, fp) != NULL)
    { // Lecture du fichier car il n'est pas vide

        char *endDayHour = calloc (S_HOUR, sizeof (char));
        strncpy (endDayHour, "23:55:00", S_HOUR);

        // Stockage de la série de données du fichier.csv dans la structure de
        // donées directement
        while (
        (day < NB_DAYS) &&
        (fscanf (fp, "%d,%d,%d,%d,%d,%Lf,%Lf,%s %s", &(t[sensor].D[day].H[hour].M[0]),
                 &(t[sensor].D[day].H[hour].M[1]), &(t[sensor].D[day].H[hour].M[2]),
                 &(t[sensor].D[day].H[hour].M[3]), &(t[sensor].D[day].H[hour].M[4]), &(t[sensor].lon),
                 &(t[sensor].lat), t[sensor].D[day].date, t[sensor].D[day].H[hour].hour) != EOF))
        {

            if ((strcmp (t[sensor].D[day].H[hour].hour, endDayHour) == 0) || (hour == NB_MEASURES_DAY - 1))
            { // Soit on est la fin d'une journée soit
              // on est a la fin du tableau
                t[sensor].D[day].nbMeasures = hour + 1;
                hour                        = 0;
                day++;
            }
            else
            {
                hour++;
            }
        }
        t[sensor].D[day].nbMeasures = hour; // Stockage du nb de mesures le dernier jour
        t[sensor].nbDays            = day + 1;
        free (endDayHour);
        endDayHour = NULL;
    }
}

// _______________________________________________________________________________________________________________
// Fonctions relatives à l'ouverture et la lecture d'un dossier

void setDirectoryPath (char *argv, char *directory_path)
{ // transforme le chemin entrée en paramètre du lancement du programme en un chemin valide

    // Vérifie qu'il n'y a pas deja le "./" devant la commande entrée
    if ((argv[0] != '.') && (argv[1] != '/'))
    {
        strncpy (directory_path, "./", 2);
    }
    strncat (directory_path, argv, strlen (argv));
    if (argv[strlen (argv) - 1] != '/')
    {
        strncat (directory_path, "/", 1);
    }
}

bool directory_reader (char *directory_path, Capteur *t, int *nbSensors)
{ // Ouvre et Lit le contenu du dossier où se trouve les fichiers csv

    DIR *          folder = NULL;
    struct dirent *entry  = NULL;
    struct stat    buffer;
    char *         full_path = NULL;
    *nbSensors               = 0;

    // Ouverture du dossier identifié par directory_path
    folder = opendir (directory_path);
    if (folder == NULL)
    {
        printf ("\nUnable to read directory : %s\n\n", directory_path);
        return false;
    }

    printf ("\n\nLes données sont en cours de chargement, veuillez patienter "
            "quelques instants...    \n\n");

    while ((entry = readdir (folder)))
    {

        // Vérification que l'extension du fichier est correcte
        if (strstr (entry->d_name, ".csv") == NULL)
        {
            continue;
        }

        // Création du chemin pour atteindre le fichier identifié par full_path
        full_path = calloc (SIZE_FULL_PATH, sizeof (char));
        strncpy (full_path, directory_path, strlen (directory_path));
        strncat (full_path, entry->d_name, strlen (entry->d_name));

        lstat (full_path, &buffer);

        if (S_ISREG (buffer.st_mode) && (*nbSensors < NB_SENSORS))
        {
            // Ouverture du fichier identifié par full_path
            FILE *fp = fopen (full_path, "re"); // ouverture en mode "re" pour mettre O_CLOEXEC (clang-tidy warning)
            if (fp == NULL)
            {
                printf ("\nUnable to read file : %s\n", full_path);
            }
            else
            {
                // Stockage des données dans la SD
                SD_filler (fp, t, *nbSensors);
                (*nbSensors)++;

                // Affichage de la barre de progression
                loader (*nbSensors * 100 / NB_SENSORS);

                // Désallocation du pointeur de fichier
                fclose (fp);
            }
        }
        // Désallocation du pointeur du chemin vers le fichier
        free (full_path);
        full_path = NULL;
    }
    closedir (folder);
    printf ("\n");
    if (!(*nbSensors))
    {
        printf ("Aucun fichier au format csv n'a été trouvé dans le dossier indiqué\n\n");
        return false;
    }
    return true;
}

void loader (int value)
{ // prend une valeur entre 0 et 100 et affiche une barre de progression

    char loader[20] = "";

    for (int i = 0; i < 20; i++)
    {
        loader[i] = '.';
    }
    int j = 0;
    for (j = 0; j < value / 5; j++)
    {
        loader[j] = '=';
    }
    loader[j] = '>';

    printf ("\r[%s] %d%%", loader, value);
    fflush (stdout); // vide le buffer pour que le contenu soit affiché
                     // (normalement un /n est attendu)
}

// _______________________________________________________________________________________________________________
// Fonctions relatives au processus de recherche

bool preProcess (Capteur *t, int nbSensors, Command *c, char *strCmd, char **arguments, bool isBatch)
{ // permet d'obtenir une commande/requête valide

    // Récupère chaque arguments de la commande entrée
    int nb_words = split_string (strCmd, arguments, ' ');
    int res      = formatCmd (arguments, c, nb_words);

    bool doProcess = true, stay = true;

    while (res)
    { // La commande est invalide (fonction formatCmd)
        if (res == 1)
        {
            printf ("Erreur: le gaz demandé n'existe pas.\n");
        }
        else if (res == 2)
        {
            if ((strcmp (strCmd, "q") == 0) || (!stay))
            { // si on souhaite sortir du programme
                return false;
            }
            printf ("Erreur: la mesure demandée n'existe pas.\n");
        }

        resetCmd (c, strCmd, arguments);

        if (isBatch)
        { // Si on est en mode Batch, on doit passer à la commande suivante sans faire le processus
            res       = 0;
            doProcess = false;
        }
        else
        {
            stay     = getCmd (strCmd);
            nb_words = split_string (strCmd, arguments, ' ');
            res      = formatCmd (arguments, c, nb_words);
        }
    }

    if (doProcess)
    {
        // printCmd (*c); // Pour le débogage
        process (*c, t, nbSensors);
        resetCmd (c, strCmd, arguments);
    }
    // printf ("\033[0m"); // reset color (pour le débogage)
    return true;
}

void process (Command c, Capteur *capteurs, int nbSensors)
{ // Processus de recherche de la valeur moyenne, maximale et minimale

    int    sum = 0, tmp = 0, max = 0;
    double cpt = 0.;
    int    min = MAX_VALUE;

    for (int s = 0; s < nbSensors; s++)
    { // Structure Capteurs

        if (c.hasArea && !isInRadius (c.radius, capteurs[s], c.loc.lon, c.loc.lat))
        { // Filtre les capteurs non désirés (capteurs pas dans le radius)
            continue;
        }
        for (int d = 0; d < capteurs[s].nbDays; d++)
        { // Structure Jour

            if (c.hasOpt && cmpDate (c.opt, c.metric, capteurs[s].D[d].date))
            { // Filtre les dates non désirées
                continue;
            }
            for (int h = 0; h < capteurs[s].D[d].nbMeasures; h++)
            { // Structure Hour

                if (c.hasOpt && cmpHour (c.opt, c.metric, capteurs[s].D[d].H[h].hour))
                { // Filtre les heures non désirées
                    continue;
                }
                tmp = capteurs[s].D[d].H[h].M[c.type];
                sum += tmp;
                if (tmp > max)
                {
                    max = tmp;
                }
                if (tmp < min)
                {
                    min = tmp;
                }
                cpt++;
            }
        }
    }
    if (cpt == 0.)
    {
        min = 0;
    }
    cpt == 0. ? cpt++ : 1; // évite une division par zéro

    // Affichage du résultat
    printf ("\n%lf %d %d\n", sum / cpt, max, min);
}

// _______________________________________________________________________________________________________________
// Fonctions relatives au traitement et à l'analyse des donnéees

bool isInRadius (int r, Capteur c, long double lon, long double lat)
{ // Renvoie true si le capteur est dans le radius et false sinon

    double radius = r * 0.00001;
    if (sqrt ((double)((lon - c.lon) * (lon - c.lon) + (lat - c.lat) * (lat - c.lat))) <= radius)
    {
        return true;
    }
    return false;
}

bool cmpHour (Timestamp t_cmde, char metric[S_METRIC], char *s_SD)
{ // Renvoie false si l'heure du timestamp t_cmde correspond à l'heure de s_SD et true sinon

    if (!strcmp (metric, "global") || !strcmp (metric, "monthly") || !strcmp (metric, "daily"))
    {
        return false;
    }

    Timestamp t_SD;
    initTime (&t_SD);

    parseHour (s_SD, &t_SD);

    int sum_cmde = t_cmde.min + 100 * t_cmde.hour;
    int sum_SD   = t_SD.min + 100 * t_SD.hour;

    if (sum_cmde == sum_SD)
    {
        return false;
    }
    return true;
}

bool cmpDate (Timestamp t, char metric[S_METRIC], char *testedDate)
{ // Renvoie false si la date du timestamp t_cmde correspond à la date de s_SD et true sinon

    // Si on est en global pas besoin de continuer
    if (!strcmp (metric, "global"))
    {
        return false;
    }

    Timestamp tested;
    initTime (&tested); // met tout à 0

    parseDate (testedDate, &tested);

    int sumCmd    = t.year + 10000 * t.month;
    int sumTested = tested.year + 10000 * tested.month;

    // Le jour est important uniquement pour daily et log
    if (!strcmp (metric, "daily") || !strcmp (metric, "log"))
    {
        sumCmd += 1000000 * t.day;
        sumTested += 1000000 * tested.day;
    }

    if (sumCmd == sumTested)
    {
        return false;
    }
    return true;
}
