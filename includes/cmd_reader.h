#ifndef INCLUDES_CMD_READER_H
#define INCLUDES_CMD_READER_H
/* *************** COMMAND_READER.H ******************** */
//
//      Contient les prototypes des fonctions relatives à la réception, lecture, stockage des commandes de l'utilisateur contenues dans cmd_reader.c
//		Contient aussi toutes les librairies, les maccros et les structures de données utilisées dans cmd_reader.c
//
/* ************************************************** */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define S_COMMAND 100 // Nombre de caractères maximum dans la chaine de caractères de la commande
#define S_METRIC 20   // Nombre de caractères maximum dans la chaine de caractères metric
#define S_TYPE 20     // Nombre de caractères maximum dans la chaine de caractères type (gaz)
#define S_OPT 19 // Nombre de caractères maximum dans la chaine de caractères option (le timestamp)
#define S_LOC 100 // Nombre de caractères maximum dans la localisation (longitude latitude)
#define MAX_ARG 7 // Nombre maximum d'elements à considérer dans les fichiers csv
#define NB_GAS 5  // Nombre de gaz mesurés

static const char GAS[NB_GAS][S_TYPE + 1] = { "ozone\0", "particullate_matter\0", "carbon_monoxide\0",
                                              "sulfure_dioxide\0", "nitrogen_dioxide\0" };

// _______________________________________________________________________________________________________________
// Définition des types de données

typedef struct
{
    long double lon;
    long double lat;
} Location;

typedef struct
{
    int year, month, day, hour, min;
} Timestamp;

typedef struct
{
    char      metric[S_METRIC]; // la plus grande commande est "monthly"
    Timestamp opt; // un timestamp est de cette taille là maximum '2014-08-08 14:00:00'
    bool      hasOpt;
    int       type;
    Location  loc;
    int       radius;
    bool      hasArea;
} Command;

// _______________________________________________________________________________________________________________
// Fonctions relatives aux Timestamps

void initTime (Timestamp *);

void printTime (Timestamp, char[S_METRIC]);

void parseDate (char *, Timestamp *);

void parseHour (char *, Timestamp *);

int truncate5 (int);

// _______________________________________________________________________________________________________________
// Fonctions relatives à la localisation

void printLoc (Location);

void loccpy (char *, char *, Location *);

// _______________________________________________________________________________________________________________
// Fonctions relatives aux gaz

int typeIndex (char *);

bool isMetric (char *);

// _______________________________________________________________________________________________________________
// Fonctions relatives aux commandes

void printCmd (Command);

int formatCmd (char **, Command *, int);

void initCmd (Command *);

void resetCmd (Command *, char *, char **);

void desallocCmd (char *, char **);

bool getCmd (char *);

int split_string (char *, char **, char);

#endif
