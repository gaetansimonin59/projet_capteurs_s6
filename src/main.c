/* *************** MAIN.C ******************** */
//
//      Contient la fonction principale du projet qui rassemble et utilise les
//      fonctions de file_reader.c et celles de cmd_reader.c
//
/* ************************************************** */

#include "../includes/file_reader.h"

// _______________________________________________________________________________________________________________
// Main function

int main (int argc, char *argv[])
{
    if ((argc < 2) || (argc > 3))
    {
        printf ("\nErreur lors du lancement de l'application, vous n'avez pas saisi les bons "
                "arguments.\nVeuillez respecter : ./main repDonnées [fichier.txt]\n\n");
        return 0;
    }

    // Initialisation de la SD
    Capteur *t         = malloc (NB_SENSORS * sizeof (Capteur));
    int      nbSensors = 0;
    initSD (t);

    // Remplissage de la SD
    char *directory_path = calloc (SIZE_FULL_PATH, sizeof (char));
    setDirectoryPath (argv[1], directory_path);
    bool rightRead = directory_reader (directory_path, t, &nbSensors);
    if (!rightRead)
    {
        return 0;
    }
    free (directory_path);
    directory_path = NULL;

    // Redimentionnement de la SD
    // Pas mis en service car pose des problèmes de fuites de memoire
    // resizing (t);

    char * cmd       = calloc (S_COMMAND, sizeof (char));
    char **arguments = calloc (MAX_ARG, sizeof (char *));

    for (int i = 0; i < MAX_ARG; i++)
    {
        arguments[i] = calloc (S_COMMAND, sizeof (char));
    }

    Command c;
    initCmd (&c);
    bool again   = true;
    bool isBatch = false;

    if (argc == 3)
    { // Détection du mode batch
        isBatch = true;
    }

    while (again)
    {
        if (isBatch)
        { // Commandes Batch

            FILE *file = fopen (argv[2], "re"); // ouverture en mode "re" pour mettre O_CLOEXEC (clang-tidy warning)
            if (file == NULL)
            { // Erreur de lecture du fichier => passage en mode manuel

                printf ("\n\nUnable to read file : %s\nPassage en mode manuel\n\n", argv[2]);
                isBatch = false;
                again   = true;
                continue;
            }
            while (fgets (cmd, S_COMMAND, file) != NULL)
            {
                printf ("\nCommande: %s", cmd);
                cmd[strcspn (cmd, "\n")] = 0; // remove the '\n' character at the end of the command
                preProcess (t, nbSensors, &c, cmd, arguments, isBatch);
            }
            again = false;
            fclose (file);
        }
        else
        { // Commandes manuelles

            if (getCmd (cmd))
            {
                again = preProcess (t, nbSensors, &c, cmd, arguments, isBatch);
            }
            else
            {
                again = false;
            }
        }
    }

    // Desallocation de la SD et de la commande
    desallocCmd (cmd, arguments);
    desallocSD (t, &nbSensors);
    return 0;
}
