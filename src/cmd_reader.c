/* *************** COMMAND_READER.C ******************** */
//
//      Contient les fonctions relatives à la réception, lecture, stockage des
//      commandes de l'utilisateur
//
/* ************************************************** */

#include "../includes/cmd_reader.h"
// _______________________________________________________________________________________________________________
// Fonctions relatives aux Timestamps

void initTime (Timestamp *t)
{ // Initialise la structure Timestamp

    t->year  = 0;
    t->month = 0;
    t->day   = 0;
    t->hour  = 0;
    t->min   = 0;
}

void printTime (Timestamp t, char metric[S_METRIC])
{ // Affiche l'heure et la date de la requête entrée ainsi que son metric

    printf ("\033[1;36m%d/%d", t.year, t.month);

    if (!strcmp (metric, "daily") || !strcmp (metric, "log"))
    {
        printf ("/%d", t.day);
    }
    printf ("\033[0;36m");

    if (!strcmp (metric, "log"))
    {
        printf (" à \033[1;36m%dh%d\033[0;36m", t.hour, t.min);
    }
    printf ("\n");
}

void parseDate (char *s, Timestamp *d)
{ // Décompose et remplit la partie date de la structure Timestamp
    // s est de la forme : 'aaaa-mm-jj'

    d->year  = (int)strtol (s, &s, 10);       // reçoit "aaaa"
    d->month = (int)strtol (s + 1, &s, 10);   // reçoit "mm"
    d->day   = (int)strtol (s + 1, NULL, 10); // reçoit "mm"
    // Utiliser s+1 permet de s'affranchir des '-' qui gène à la conversion
}

void parseHour (char *s, Timestamp *d)
{ // Décompose et remplit la partie heure de la structure Timestamp
    // s est de la forme : 'hh:mm:ss'

    d->hour = (int)strtol (s, &s, 10);
    d->min  = (int)strtol (s + 1, NULL, 10);
    d->min  = truncate5 (d->min);
    // Utiliser s+1 permet de s'affranchir des ':' qui gène à la conversion
}

int truncate5 (int i)
{
    // Permet de tronquer la valeur de i sur 5
    // Utiliser pour avoir '23:56:47' -> '23:55:00'

    int a = i / 5;
    return a * 5;
}

// _______________________________________________________________________________________________________________
// Fonctions relatives à la localisation

void printLoc (Location l)
{ // Affiche la localisation du capteur

    printf ("%Lf\t%Lf\n", l.lon, l.lat);
}

void loccpy (char *lon, char *lat, Location *l)
{ // Transforme en long double et stocke dans la structure Location les strings de longitude et latitude
    l->lon = strtold (lon, NULL);
    l->lat = strtold (lat, NULL);
}


// _______________________________________________________________________________________________________________
// Fonctions relatives aux gaz

int typeIndex (char *gas)
{ // retrouve l'index du gaz
    // les gaz sont définis dans une constante de cmd_reader.h, elle a la forme :
    // char gas[NB_GAS][S_TYPE] =
    // {"ozone","particullate_matter","carbon_monoxide","sulfure_dioxide","nitrogen_dioxide"};

    for (int i = 0; i < NB_GAS; i++)
    {
        if (!strcmp (gas, GAS[i]))
        {
            return i;
        }
    }
    return -1;
}

bool isMetric (char *word)
{ // Détermine si le metric reçu est valide et renvoie true si c'est le cas et false sinon

    if (!strcmp (word, "global") || !strcmp (word, "monthly") || !strcmp (word, "daily") || !strcmp (word, "log"))
    {
        return true;
    }
    return false;
}

// _______________________________________________________________________________________________________________
// Fonctions relatives aux commandes

void printCmd (Command cmd)
{ // Affiche la commande entrée de façon décomposée

    printf ("\033[0;36m"); // start blue printing
    printf ("\n\033[1;36m Commande \033[1;36m:\n");
    printf ("metric: \t\033[0;36m%s\033[1;36m\n", cmd.metric);
    printf ("type:   \t\033[0;36m%s\033[1;36m\n", GAS[cmd.type]);
    printf ("has opt:\t%s\n", cmd.hasOpt ? "\033[1;32mtrue\033[0;36m" : "\033[1;31mfalse \033[0;36m");
    if (cmd.hasOpt)
    {
        printf ("\033[1;36mopt:\033[0;36m    \t");
        printTime (cmd.opt, cmd.metric);
    }
    printf ("\033[1;36mhas area:\033[0;36m\t%s\n",
            cmd.hasArea ? "\033[1;32mtrue\033[0;36m" : "\033[1;31mfalse \033[0;36m");
    if (cmd.hasArea)
    {
        printf ("\033[1;36mloc:\033[0;36m    \t");
        printLoc (cmd.loc);
        printf ("\033[1;36mradius:\033[0;36m \t%d\n\n", cmd.radius);
    }
    printf ("\033[0m"); // reset color
}

int formatCmd (char **arguments, Command *command, int nbwords)
{
    /*formate un tableau de mots en une structure commande
    renvoie 1 pour une erreur sur 'type'
    renvoie 2 pour une erreur sur 'metric'
    */

    int gas = -1;

    initCmd (command);

    strncpy (command->metric, arguments[0], strlen (arguments[0]));

    if (!strcmp (command->metric, "global"))
    {
        gas = typeIndex (arguments[1]);

        if (gas == -1)
        {
            return 1; // signifie qu'il y a une erreur
        }

        command->type    = gas;
        command->hasArea = false;

        if (nbwords == 2)
        {
            return 0;
        }
        command->hasArea = true;
        loccpy (arguments[2], arguments[3], &command->loc);
        command->radius = (int)strtol (arguments[4], NULL, 10);
    }
    else if (!strcmp (command->metric, "monthly") || !strcmp (command->metric, "daily"))
    {
        command->hasOpt = true;
        Timestamp time;
        initTime (&time);
        parseDate (arguments[1], &time);
        command->opt = time;

        gas = typeIndex (arguments[2]);
        if (gas == -1)
        {
            return 1; // signifie qu'il y a une erreur
        }

        command->type    = gas;
        command->hasArea = false;
        if (nbwords == 3)
        {
            return 0;
        }
        command->hasArea = true;
        loccpy (arguments[3], arguments[4], &command->loc);
        command->radius = (int)strtol (arguments[5], NULL, 10);
    }
    else if (!strcmp (command->metric, "log"))
    { // metric == log

        gas = typeIndex (arguments[3]);
        if (gas == -1)
        {
            return 1; // signifie qu'il y a une erreur
        }

        command->type   = gas;
        command->hasOpt = true;
        Timestamp time;
        initTime (&time);
        parseDate (arguments[1], &time);
        parseHour (arguments[2], &time);
        command->opt = time;

        command->hasArea = false;
        if (nbwords == 4)
        {
            return 0;
        }
        command->hasArea = true;
        loccpy (arguments[4], arguments[5], &command->loc);
        command->radius = (int)strtol (arguments[6], NULL, 10);
    }
    else
    {
        return 2;
    }
    return 0;
}

void initCmd (Command *c)
{ // Initialise la structure Command

    memset (&(c->metric), 0, S_METRIC); // reset function for a static array
    memset (&(c->type), 0, S_TYPE);
    c->hasOpt = false;
    initTime (&(c->opt));
    c->hasArea = false;
    c->loc.lon = (long double)0.;
    c->loc.lat = (long double)0.;
    c->radius  = 0;
}

void resetCmd (Command *c, char *cmd, char **arguments)
{ // Ré-initialise toutes les variables relatives au traitement d'une commande/requête utilisateur

    // Reset la structure Commande
    initCmd (c);

    // Reset la commande
    free (cmd);
    cmd = calloc (S_COMMAND, sizeof (char));

    // Reset le tableau d'arguments
    for (int i = 0; i < MAX_ARG; i++)
    {
        free (arguments[i]);
        arguments[i] = calloc (S_COMMAND, sizeof (char));
    }
}

void desallocCmd (char *cmd, char **arguments)
{ // Désalloue une commande

    for (int i = 0; i < MAX_ARG; i++)
    {
        free (arguments[i]);
        arguments[i] = NULL;
    }
    free (arguments);
    arguments = NULL;
    free (cmd);
    cmd = NULL;
}

bool getCmd (char *cmd)
{ // Permet de recevoir la commande de l'utilisateur dans la string cmd

    printf ("\nEntrez une commande : metric [opt] type [longitude latitude "
            "radius]\n");
    if (fgets (cmd, S_COMMAND - 1, stdin) == NULL)
    {
        return false; // we use fgets instead of scanf to allow spaces in a command
    }
    cmd[strcspn (cmd, "\n")] = 0; // remove the '\n' character at the end of the command
    return true;
}

int split_string (char *s, char **split_tab, char c)
{
    /*
    splits a string in an array
    returns the amount of 'words'
    */

    int w_size = 0, compt = 0, s_size = (int)strlen (s);

    for (int i = 0; i < s_size; i++)
    {
        if (s[i] != c && s[i] != '\0')
        {
            split_tab[w_size][compt] = s[i];
            compt++;
        }
        else if (s[i] != '\0')
        {
            w_size++;
            compt = 0;
        }
    }
    return w_size + 1;
}
