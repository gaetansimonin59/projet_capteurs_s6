#ifndef INCLUDES_FILE_READER_H
#define INCLUDES_FILE_READER_H

/* *************** FILE_READER.H ******************** */
//
//      Contient les prototypes des fonctions relatives à l'ouverture, la lecture et le stockage des données des fichiers .csv
//		Contient aussi toutes les librairies, les maccros et les structures de données utilisées dans file_reader.c
//
/* ************************************************** */

#include "cmd_reader.h"

#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#define S_FIRST_LINE 107 // Nombre de caractères sur la première ligne des fichiers csv
#define S_DATE 10        // Nombre de caractères qui compose la date ('aaaa-mm-jj')
#define S_HOUR 8         // Nombre de caractères qui compose la date ('hh:mm:ss')
#define NB_SENSORS 449   // Nombre de maximum de capteurs et donc de fichiers.csv
#define NB_DAYS 62       // Nombre de jours maximum pour une session de relevés de 2 mois
#define NB_MEASURES_DAY \
    288 // Nombre de relevés maximum pour une journée (un relevé toutes les 5 mins)
#define SIZE_FULL_PATH \
    100 // Nombre de caractères de la chaine qui contient le chemin complet vers un fichier
#define MAX_VALUE 300 // Valeur maximale d'une mesure (dans les fichiers le max est 215)

// _______________________________________________________________________________________________________________
// Definition des structures et types de données

typedef struct
{
    int * M; // ozone,particullate_matter,carbon_monoxide,sulfure_dioxide,nitrogen_dioxide
    char *hour;
} Hour;

typedef struct
{
    Hour *H;
    char *date;
    int   nbMeasures; // Stocke la taille de H
} Day;

typedef struct
{
    Day *       D;
    long double lon,
    lat; // avec des pointeurs sur long double on enlève le warning "padding struct 'Capteur' with 8
         // bytes to align 'lon' [-Wpadded]" Mais alors tres tres grosse flmmme de tout changer
    int nbDays; // Stocke la taille de D
} Capteur;

// _______________________________________________________________________________________________________________
// Fonctions relatives à la Structure de Données

void initSD (Capteur *);

void desallocSD (Capteur *, int *);

void resizing (Capteur *, int);

void SD_filler (FILE *, Capteur *, int);

// _______________________________________________________________________________________________________________
// Fonctions relatives à l'ouverture et la lecture d'un dossier

void setDirectoryPath (char *, char *);

bool directory_reader (char *, Capteur *, int *);

void loader (int);

// _______________________________________________________________________________________________________________
// Fonctions relatives au processus de recherche et de calcul

bool preProcess (Capteur *, int, Command *, char *, char **, bool);

void process (Command, Capteur *, int);

// _______________________________________________________________________________________________________________
// Fonctions relatives au traitement et à l'analyse des donnéees

bool isInRadius (int, Capteur, long double, long double);

bool cmpHour (Timestamp, char[S_METRIC], char *);

bool cmpDate (Timestamp, char[S_METRIC], char *);

#endif
